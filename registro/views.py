from django.shortcuts import redirect,render
from django.utils import timezone
from django.http import HttpResponse
from .models import Perrito, Persona
from .forms import PersonaForm
from .forms import UsuarioForm
# Create your views here.

#importar user
from django.contrib.auth.models import User
#sistema de autenticación 
from django.contrib.auth import authenticate,logout, login as auth_login

from django.contrib.auth.decorators import login_required

def index(request):
    return render(request, 'index.html', {})

def login(request):
    return render(request, 'login.html', {})

def user(request):
    return render(request, 'user.html', {})

def admin(request):
    return render(request, 'admin.html', {})

def cuentas(request):
    return render(request, 'accounts.html', {})

def inicio(request):
    return render(request, 'index.html', {})

def registro(request):
    return render(request, 'registro.html', {})

def contacto(request):
    return render(request, 'contact.html', {})

def registro2(request):
    if request.method == "POST":
        form = PersonaForm(request.POST)
        if form.is_valid():
            usuario = form.cleaned_data['email']
            password = form.cleaned_data['contrasenia']

            try:
                user_val = User.objects.get(
                    username=form.cleaned_data['email'])
            except User.DoesNotExist:
                user_val = None

            if user_val is None:
                user = User.objects.create_user(usuario, usuario, password)
                user.save()
                persona = form.save(commit=False)
                persona.usuario = user
                persona.save()
                return redirect('completo', pk=user.id)
        return render(request, 'registro.html', {'form': form})
    else:
        form = PersonaForm()
        return render(request, 'registro.html', {'form': form})

def acceso(request):
    if request.method == "POST":
        form = UsuarioForm(request.POST)
        user = None
        usuario = request.POST['username']
        password = request.POST['password']
        print(usuario)
        print(password)
        user = authenticate(request, username=usuario, password=password)
        print(user)          
        if user is not None:
            auth_login(request, user)
            return redirect("index")
        else:
            return render(request, 'login.html', {'form': form})
    else:
        form = UsuarioForm()
        return render(request, 'login.html', {'form': form})

def cerrar_session(request):
    logout(request)
    return redirect('index.html')


def completo(request, pk):
    return render(request, 'completo.html', {})

# def crearP(request):
#     nombre = request.POST.get('nombre','')
#     raza = request.POST.get('raza','')
#     descripcion = request.POST.get('descripcion','')
#     perrito = Perrito(nombre = nombre, raza=raza, descripcion=descripcion)
#     perrito.save()
#     return redirect('index.html')    

# def crear(request):
#     nombre = request.POST.get('nombre','')
#     apellido = request.POST.get('apellido','')
#     correo = request.POST.get('correo','')
#     contrasenia = request.POST.get('contrasenia','')
#     telefono = request.POST.get('telefono','')
#     rut = request.POST.get('rut','')
#     persona = Persona(nombre=nombre,correo=correo,contrasenia=contrasenia)
#     persona.save()
#     return redirect('index')
    