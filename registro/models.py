#from _future_ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Perrito(models.Model):
    nombre = models.CharField(max_length=30)
    raza = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=100)
    imagen = models.ImageField(upload_to='mi-perris-e2/foto_perro', blank=True)

    def __str__(self):
        return "PERRITO" 

class Persona(models.Model):    
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(blank=False, max_length=100, primary_key=True)
    contrasenia = models.CharField(blank=False, max_length=20, default=None)
    run = models.CharField(max_length=12)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)    
    telefono = models.IntegerField()
    

    def __str__(self):
        return self.nombre

