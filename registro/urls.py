from django.urls import path
from . import views

urlpatterns = [
    
    path('',views.index,name="index"),
    path('inicio',views.index,name="inicio"),
    path('login',views.login, name="login"),
    path('user',views.user, name="user"),
    path('admin',views.admin, name="admin"),
    path('contacto',views.contacto, name="contacto"),
    path('registro',views.registro, name="registro"),
    path('cuentas',views.cuentas, name="cuentas"),
    path('cerrarsession',views.cerrar_session,name="cerrar_session"),
    path('completo/<int:pk>',views.completo, name='completo')
]