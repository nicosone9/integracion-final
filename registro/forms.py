from django import forms
from .models import Persona
from django.contrib.auth.models import User

class PersonaForm(forms.ModelForm):

    class Meta:
        model = Persona
        fields = ('email','contrasenia','nombre','apellido','run','telefono')


class UsuarioForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password',)